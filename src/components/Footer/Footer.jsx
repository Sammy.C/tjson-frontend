/*!

=========================================================
* BLK Design System React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/blk-design-system-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/blk-design-system-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import { Link } from "react-router-dom";
// reactstrap components
import {
  NavItem,
  NavLink,
  Nav,
  Container,
  Row,
  Col,
} from "reactstrap";

class Footer extends React.Component {
  render() {
    return (
      <footer className="footer">
        <Container>
          <Row>
            <Col md="4">
              <h1 className="title">T•Json</h1>
            </Col>
            <Col md="4">
              <Nav>
                <NavItem>
                  <NavLink to="/" tag={Link}>
                    Home
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink to="/translate" tag={Link}>
                    Translate
                  </NavLink>
                </NavItem>
              </Nav>
            </Col>
            <Col md="4">
              <Nav>
                <NavItem>
                  <NavLink href = "mailto:translationjson@gmail.com">
                    Contact Us
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=translationjson%40gmail.com&item_name=Open+Source+Project&currency_code=USD&source=url" >
                    Donate
                  </NavLink>
                </NavItem>
              </Nav>
            </Col>
          </Row>
        </Container>
      </footer>
    );
  }
}

export default Footer;
