/*!

=========================================================
* BLK Design System React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/blk-design-system-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/blk-design-system-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import classnames from "classnames";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Row,
  Col,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
  Modal
} from "reactstrap";

import "jsoneditor-react/es/editor.min.css";
import ExamplesNavbar from "components/Navbars/NavBar.jsx";
import Footer from "components/Footer/Footer.jsx";
import axios from "axios";
import Files from "react-files";
import ReactJson from "./JsonEditor.jsx";

class TranslatePage extends React.Component {
  constructor(props) {
    super(props);

    this.toggleModalLarge = this.toggleModalLarge.bind(this);
    this.state = this.initialState();
    this.fileReader = new FileReader();
    this.fileReader.onload = event => {
      this.setState({ jsonTranslated: "" });
      this.setState({
        languageFrom: "en",
        jsonToTranslate: JSON.parse(event.target.result)
      });
      this.toggleModalLarge();
    };
  }
  initialState() {
    return {
      squares1to6: "",
      squares7and8: "",
      iconTabs: 1,
      textTabs: 4,
      modalTab: 1,
      showLoader: false,
      languageFrom: "",
      jsonToTranslate: "",
      jsonTranslated: "",
      src: {},
      currentKey: "",
      currentValue: "",
      languages: {
        en: false,
        fr: false,
        ja: false,
        de: false,
        zh: false,
        es: false,
        pt: false,
        it: false,
        ru: false
      }
    };
  }
  toggleModalLarge() {
    this.setState({
      modalLarge: !this.state.modalLarge,
      languages: {
        en: false,
        fr: false,
        ja: false,
        de: false,
        zh: false,
        es: false,
        pt: false,
        it: false,
        ru: false
      },
      showLoader: false,
      modalTab: 1,
      languageFrom: ""
    });
  }
  toggleLanguage = key => {
    let val = !this.state.languages[key];
    let obj = this.state.languages;
    obj[key] = val;
    this.setState({ languages: obj });
  };
  addJson = e => {
    if (
      this.state.currentKey.trim() === "" ||
      this.state.currentValue.trim() === ""
    ) {
      return;
    } else {
      let obj = this.state.src;
      obj[this.state.currentKey] = this.state.currentValue;
      this.setState({ json_object: obj });
      this.setState({ currentKey: "", currentValue: "" });
    }
  };
  toggleTabs = (e, stateName, index) => {
    e.preventDefault();
    this.setState({
      [stateName]: index
    });
  };
  componentDidMount() {
    document.body.classList.toggle("register-page");
  }
  componentWillUnmount() {
    document.body.classList.toggle("register-page");
  }
  onClickHandle = index => {
    console.log(index);
    let obj = this.state.src;
    delete obj[index];
    this.setState({ src: obj });
  };
  makeApiCall() {
    let languagesTo = [];

    for (let language in this.state.languages) {
      if (this.state.languages[language]) {
        languagesTo.push(language);
      }
    }
    this.setState({ showLoader: true });
    axios
      .post("http://localhost:3000/api/translate", {
        jsonToTranslate: this.state.jsonToTranslate,
        from: this.state.languageFrom,
        to: languagesTo
      })
      .then(res => {
        this.setState({ jsonTranslated: res.data });
      });
  }

  renderDownloadLinks() {
    let items = [];
    const jsonTranslated = this.state.jsonTranslated;

    for (let language in jsonTranslated) {
      let dataStr =
        "data:text/json;charset=utf-8," +
        encodeURIComponent(JSON.stringify(jsonTranslated[language]));
      items.push(
        <div key={language} className="text-center ml-auto mr-auto">
          <h3 className="text-center ml-auto mr-auto">
            <a
              className="text-center ml-auto mr-auto text-info"
              href={"data:" + dataStr}
              download={language + ".json"}
            >
              Download {language}.json
            </a>
          </h3>
        </div>
      );
    }

    return <div>{items}</div>;
  }

  render() {
    window.scrollTo(0, 0);

    let downloadLink =
      this.state.jsonTranslated === "" ? null : this.renderDownloadLinks();
    const obj = this.state.src;
    const showLoader = this.state.showLoader;
    let nextButton = null;
    let modalBody = null;
    if (!isEmpty(obj)) {
      nextButton = (
        <Button
          color="info"
          onClick={e => {
            this.toggleModalLarge();
            this.setState({
              jsonToTranslate: obj,
              jsonTranslated: ""
            });
          }}
        >
          Next
        </Button>
      );
    }
    if (showLoader) {
      modalBody = (
        <div>
          <div className="modal-header justify-content-center">
            <button className="close" onClick={this.toggleModalLarge}>
              <i className="tim-icons icon-simple-remove text-white" />
            </button>
            <div className="text-muted text-center ml-auto mr-auto">
              <h2 className="mb-0">We Are Making Your Json Files</h2>
            </div>
          </div>
          <div className="modal-body  justify-content-center align-items-center">
            <Row>
              <img
                alt="..."
                className="mx-auto d-block"
                src={require("assets/img/Infinity-1s-200px.svg")}
              />
            </Row>
          </div>
        </div>
      );
    } else {
      modalBody = (
        <TabContent
          className="tab-space"
          activeTab={"tab" + this.state.modalTab}
        >
          <TabPane tabId="tab1">
            <div>
              <div className="modal-header justify-content-center">
                <button className="close" onClick={this.toggleModalLarge}>
                  <i className="tim-icons icon-simple-remove text-white" />
                </button>
                <div className="text-muted text-center ml-auto mr-auto">
                  <h2 className="mb-0">Choose Your Base Language</h2>
                </div>
              </div>
              <div className="modal-body  justify-content-center align-items-center">
                <Row>
                  <Col
                    lg="4"
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Button
                      className="btn-simple"
                      color={this.state.languageFrom === "en" ? "primary" : ""}
                      onClick={e => this.setState({ languageFrom: "en" })}
                    >
                      English
                </Button>
                  </Col>
                  <Col
                    lg="4"
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Button
                      className="btn-simple"
                      color={this.state.languageFrom === "fr" ? "primary" : ""}
                      onClick={e => this.setState({ languageFrom: "fr" })}
                    >
                      French
                </Button>
                  </Col>
                  <Col
                    lg="4"
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Button
                      className="btn-simple"
                      color={this.state.languageFrom === "ja" ? "primary" : ""}
                      onClick={e => this.setState({ languageFrom: "ja" })}
                    >
                      Japanese
                </Button>
                  </Col>
                  <Col
                    lg="4"
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Button
                      className="btn-simple"
                      color={this.state.languageFrom === "de" ? "primary" : ""}
                      onClick={e => this.setState({ languageFrom: "de" })}
                    >
                      German
                </Button>
                  </Col>

                  <Col
                    lg="4"
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Button
                      className="btn-simple"
                      color={this.state.languageFrom === "zh" ? "primary" : ""}
                      onClick={e => this.setState({ languageFrom: "zh" })}
                    >
                      Chinese
                </Button>
                  </Col>
                  <Col
                    lg="4"
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Button
                      className="btn-simple"
                      color={this.state.languageFrom === "es" ? "primary" : ""}
                      onClick={e => this.setState({ languageFrom: "es" })}
                    >
                      Spanish
                </Button>
                  </Col>
                  <Col
                    lg="4"
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Button
                      className="btn-simple"
                      color={this.state.languageFrom === "pt" ? "primary" : ""}
                      onClick={e => this.setState({ languageFrom: "pt" })}
                    >
                      Portuguese
                </Button>
                  </Col>
                  <Col
                    lg="4"
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Button
                      className="btn-simple"
                      color={this.state.languageFrom === "it" ? "primary" : ""}
                      onClick={e => this.setState({ languageFrom: "it" })}
                    >
                      Italian
                </Button>
                  </Col>
                  <Col
                    lg="4"
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Button
                      className="btn-simple"
                      color={this.state.languageFrom === "ru" ? "primary" : ""}
                      onClick={e => this.setState({ languageFrom: "ru" })}
                    >
                      Russian
                </Button>
                  </Col>
                  <Col lg="12" className="mt-5">
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <Button
                        color="primary"
                        onClick={e => {
                          this.toggleTabs(e, "modalTab", 2);
                        }}
                      >
                        Next
                  </Button>
                    </div>
                  </Col>
                </Row>
              </div>
            </div>
          </TabPane>
          <TabPane tabId="tab2">
            <div>
              <div className="modal-header justify-content-center">
                <button className="close" onClick={this.toggleModalLarge}>
                  <i className="tim-icons icon-simple-remove text-white" />
                </button>
                <div className="text-muted text-center ml-auto mr-auto">
                  <h2 className="mb-0">Choose The Languages You Wish To Translate</h2>
                </div>
              </div>
              <div className="modal-body  justify-content-center align-items-center">
                <Row>
                  <Col
                    lg="4"
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Button
                      className="btn-simple"
                      color={this.state.languages["en"] ? "primary" : ""}
                      onClick={e => this.toggleLanguage("en")}
                    >
                      English
                </Button>
                  </Col>
                  <Col
                    lg="4"
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Button
                      className="btn-simple"
                      color={this.state.languages["fr"] ? "primary" : ""}
                      onClick={e => this.toggleLanguage("fr")}
                    >
                      French
                </Button>
                  </Col>
                  <Col
                    lg="4"
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Button
                      className="btn-simple"
                      color={this.state.languages["ja"] ? "primary" : ""}
                      onClick={e => this.toggleLanguage("ja")}
                    >
                      Japanese
                </Button>
                  </Col>
                  <Col
                    lg="4"
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Button
                      className="btn-simple"
                      color={this.state.languages["de"] ? "primary" : ""}
                      onClick={e => this.toggleLanguage("de")}
                    >
                      German
                </Button>
                  </Col>

                  <Col
                    lg="4"
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Button
                      className="btn-simple"
                      color={this.state.languages["zh"] ? "primary" : ""}
                      onClick={e => this.toggleLanguage("zh")}
                    >
                      Chinese
                </Button>
                  </Col>
                  <Col
                    lg="4"
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Button
                      className="btn-simple"
                      color={this.state.languages["es"] ? "primary" : ""}
                      onClick={e => this.toggleLanguage("es")}
                    >
                      Spanish
                </Button>
                  </Col>
                  <Col
                    lg="4"
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Button
                      className="btn-simple"
                      color={this.state.languages["pt"] ? "primary" : ""}
                      onClick={e => this.toggleLanguage("pt")}
                    >
                      Portuguese
                </Button>
                  </Col>
                  <Col
                    lg="4"
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Button
                      className="btn-simple"
                      color={this.state.languages["it"] ? "primary" : ""}
                      onClick={e => this.toggleLanguage("it")}
                    >
                      Italian
                </Button>
                  </Col>
                  <Col
                    lg="4"
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Button
                      className="btn-simple"
                      color={this.state.languages["ru"] ? "primary" : ""}
                      onClick={e => this.toggleLanguage("ru")}
                    >
                      Russian
                </Button>
                  </Col>
                  <Col lg="12" className="mt-5">
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <Button
                        color="primary"
                        onClick={e => {
                          this.makeApiCall();
                        }}
                      >
                        Finish
                  </Button>
                    </div>
                  </Col>
                </Row>
              </div>
            </div>
          </TabPane>
        </TabContent>

      );
    }
    if (downloadLink != null) {
      modalBody = (
        <div>
          <div className="modal-header justify-content-center">
            <button className="close" onClick={this.toggleModalLarge}>
              <i className="tim-icons icon-simple-remove text-white" />
            </button>
            <div className="text-muted text-center ml-auto mr-auto">
              <h2 className="mb-0">Your Files Are Ready</h2>
            </div>
          </div>
          <div className="modal-body  justify-content-center align-items-center">
            {downloadLink}
          </div>
        </div>
      );
    }

    return (
      <>
        <ExamplesNavbar />
        <div className="wrapper">
          <div className="page-header">
            <div className="page-header-image" />
            <div className="content">
              <Container>
                <Row>
                  <Col className="offset-lg-0 offset-md-3" lg="12" md="6">
                    <div
                      className="square square-7"
                      id="square7"
                      style={{ transform: this.state.squares7and8 }}
                    />
                    <div
                      className="square square-8"
                      id="square8"
                      style={{ transform: this.state.squares7and8 }}
                    />

                    <Card className="card-register">
                      <CardHeader>
                        <CardTitle className=" text-white text-center" tag="h5">
                          Translate
                        </CardTitle>
                        <Nav
                          className="nav-tabs-info justify-content-center align-items-center"
                          role="tablist"
                          tabs
                        >
                          <NavItem>
                            <NavLink
                              className={classnames({
                                active: this.state.textTabs === 4
                              })}
                              onClick={e => {
                                this.toggleTabs(e, "textTabs", 4);
                                this.setState({ jsonTranslated: "" });
                              }}
                              href="#pablo"
                            >
                              Upload Json File
                            </NavLink>
                          </NavItem>
                          <NavItem>
                            <NavLink
                              className={classnames({
                                active: this.state.textTabs === 5
                              })}
                              onClick={e => {
                                this.toggleTabs(e, "textTabs", 5);
                                this.setState({ jsonTranslated: "" });
                              }}
                              href="#pablo"
                            >
                              Create My Json File
                            </NavLink>
                          </NavItem>
                        </Nav>
                      </CardHeader>

                      <CardBody>
                        <TabContent
                          className="tab-space"
                          activeTab={"link" + this.state.textTabs}
                        >
                          <TabPane tabId="link4">
                            <div
                              style={{
                                display: "flex",
                                justifyContent: "center",
                                alignItems: "center"
                              }}
                            >
                              <Files
                                className="files-dropzone"
                                onChange={files => {
                                  this.fileReader.readAsText(
                                    files[files.length - 1]
                                  );
                                }}
                                accepts={[".json"]}
                                clickable
                              >
                                Drop files here or click to upload
                              </Files>
                            </div>
                          </TabPane>
                          <TabPane tabId="link5">
                            <Row>
                              <Col lg="12">
                                <ReactJson
                                  src={obj}
                                  onClickHandle={this.onClickHandle}
                                />
                              </Col>
                              <Col lg="12" className="mt-3">
                                <Row>
                                  <Col lg="5">
                                    <InputGroup
                                      className={classnames({
                                        "input-group-focus": this.state
                                          .fullNameFocus
                                      })}
                                    >
                                      <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                          <i className="tim-icons icon-key-25" />
                                        </InputGroupText>
                                      </InputGroupAddon>
                                      <Input
                                        placeholder="Key"
                                        type="text"
                                        onFocus={e =>
                                          this.setState({ fullNameFocus: true })
                                        }
                                        onBlur={e =>
                                          this.setState({
                                            fullNameFocus: false
                                          })
                                        }
                                        onChange={e => {
                                          this.setState({
                                            currentKey: e.target.value
                                          });
                                        }}
                                        value={this.state.currentKey}
                                      />
                                    </InputGroup>
                                  </Col>
                                  <Col lg="5">
                                    <InputGroup
                                      className={classnames({
                                        "input-group-focus": this.state
                                          .emailFocus
                                      })}
                                    >
                                      <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                          <i className="tim-icons icon-pencil" />
                                        </InputGroupText>
                                      </InputGroupAddon>
                                      <Input
                                        placeholder="Value"
                                        type="text"
                                        onFocus={e =>
                                          this.setState({ emailFocus: true })
                                        }
                                        onBlur={e =>
                                          this.setState({ emailFocus: false })
                                        }
                                        onChange={e => {
                                          this.setState({
                                            currentValue: e.target.value
                                          });
                                        }}
                                        value={this.state.currentValue}
                                      />
                                    </InputGroup>
                                  </Col>
                                  <Col lg="2">
                                    <InputGroup
                                      className={classnames({
                                        "input-group-focus": this.state
                                          .emailFocus
                                      })}
                                    >
                                      <Button
                                        color="primary"
                                        onClick={this.addJson}
                                      >
                                        <i className="tim-icons icon-check-2" />
                                      </Button>
                                    </InputGroup>
                                  </Col>
                                </Row>
                              </Col>
                              <Col lg="12" className="mt-5">
                                <div
                                  style={{
                                    display: "flex",
                                    justifyContent: "center",
                                    alignItems: "center"
                                  }}
                                >
                                  {nextButton}
                                </div>
                              </Col>
                            </Row>
                          </TabPane>
                        </TabContent>
                      </CardBody>
                    </Card>
                  </Col>
                </Row>
                <Modal
                  modalClassName="modal-black"
                  isOpen={this.state.modalLarge}
                  toggle={this.toggleModalLarge}
                >
                  {modalBody}
                </Modal>
                <div
                  className="square square-1"
                  id="square1"
                  style={{ transform: this.state.squares1to6 }}
                />
                <div
                  className="square square-2"
                  id="square2"
                  style={{ transform: this.state.squares1to6 }}
                />
                <div
                  className="square square-3"
                  id="square3"
                  style={{ transform: this.state.squares1to6 }}
                />
                <div
                  className="square square-4"
                  id="square4"
                  style={{ transform: this.state.squares1to6 }}
                />
                <div
                  className="square square-5"
                  id="square5"
                  style={{ transform: this.state.squares1to6 }}
                />
                <div
                  className="square square-6"
                  id="square6"
                  style={{ transform: this.state.squares1to6 }}
                />
              </Container>
            </div>
          </div>
          <Footer />
        </div>
      </>
    );
  }
}
function isEmpty(obj) {
  for (var key in obj) {
    if (obj.hasOwnProperty(key)) return false;
  }
  return true;
}
export default TranslatePage;
