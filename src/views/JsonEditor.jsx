import React from "react";
// reactstrap components
import {
  Button,
} from "reactstrap";

class JsonEditor extends React.Component {
  state = {
    src: {}
  };

  componentWillMount() {
    this.setState({ src: this.props.src });
  }
  

  render() {
    const obj = this.state.src;
    return (
      <div
        className="mb-2 p-2"
        style={{ border: "2px solid #e14eca", borderRadius: "10px" }}
      >
        <div className="ml-2">
          <p>{"{"}</p>
          {Object.keys(obj).map((keyName, i) => (
            <div className="ml-3 mb-1" key={i}>
              <span className="input-label text-white">
                 {keyName}: {obj[keyName]}
              </span>
              <Button
                className="btn-round btn-icon ml-1"
                color="primary"
                onClick={e => this.props.onClickHandle(keyName)}
                style={{
                  height: " 1.175rem",
                  minWidth: "1.175rem",
                  width: "1.175rem"
                }}
              >
                <i className="tim-icons icon-simple-remove" />
              </Button>
            </div>
          ))}
          <p>{"}"}</p>
        </div>
      </div>
    );
  }
}

export default JsonEditor;
